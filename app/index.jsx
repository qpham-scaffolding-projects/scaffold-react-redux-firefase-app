import React from 'react';
import ReactDOM from 'react-dom';

import './styles.scss';

const App = () => <h1>Hello</h1>;

ReactDOM.render(<App />, document.getElementById('root'));
